﻿using Domain.Model.Base;

namespace Domain.Model
{
    public class Category : BaseEntity
    {
        public string Name { get; set; }
    }
}
